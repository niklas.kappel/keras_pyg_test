import os

os.environ["KERAS_BACKEND"] = "torch"
import keras
import torch
import torch.nn.functional as F
from keras import layers
from torch.nn import Linear
from torch_geometric.datasets import TUDataset
from torch_geometric.loader import DataLoader
from torch_geometric.nn import GCNConv, global_mean_pool

dataset = TUDataset(root="data/TUDataset", name="MUTAG")
train_dataset = dataset[:150]
test_dataset = dataset[150:]
train_loader = DataLoader(train_dataset, batch_size=64, shuffle=True)  # type: ignore
test_loader = DataLoader(test_dataset, batch_size=64, shuffle=False)  # type: ignore


class GCN(torch.nn.Module):
    def __init__(self, hidden_channels):
        super(GCN, self).__init__()
        torch.manual_seed(12345)
        self.conv1 = GCNConv(dataset.num_node_features, hidden_channels)
        self.conv2 = GCNConv(hidden_channels, hidden_channels)
        self.conv3 = GCNConv(hidden_channels, hidden_channels)
        self.lin = Linear(hidden_channels, dataset.num_classes)

    def forward(self, x, edge_index, batch):
        # 1. Obtain node embeddings
        x = self.conv1(x, edge_index)
        x = x.relu()
        x = self.conv2(x, edge_index)
        x = x.relu()
        x = self.conv3(x, edge_index)

        # 2. Readout layer
        x = global_mean_pool(x, batch)  # [batch_size, hidden_channels]

        # 3. Apply a final classifier
        x = F.dropout(x, p=0.5, training=self.training)
        x = self.lin(x)

        return x


def make_keras_model_functional():
    # This does not work, because keras expects a shape argument.
    # This should be the shape of a single sample excluding the batch dimension.
    # For PyG graphs, this does not make sense because they don't have a batch dimension,
    # and inidividual samples can have different node and edge tensor shapes.
    inputs = keras.Input()  # ValueError: You must pass a `shape` argument.
    layer = layers.TorchModuleWrapper(GCN(hidden_channels=64))
    outputs = layer(inputs)
    model = keras.Model(inputs=inputs, outputs=outputs)
    return model


def make_keras_model_sequential():
    # This also doesn't work.
    # When calling model.fit, keras raises:
    # AttributeError: 'GlobalStorage' object has no attribute 'shape'
    model = keras.Sequential()
    model.add(layers.TorchModuleWrapper(GCN(hidden_channels=64)))
    return model


# This also doesn't work.
# When calling model.fit, keras raises:
# AttributeError: 'GlobalStorage' object has no attribute 'shape'
class MyModel(keras.Model):
    def __init__(self):
        super().__init__()
        self.layer = layers.TorchModuleWrapper(GCN(hidden_channels=64))

    def call(self, inputs):  # type: ignore
        return self.layer(inputs)


model = MyModel()
model.compile(
    optimizer=keras.optimizers.Adam(), loss=keras.losses.CategoricalCrossentropy()
)
model.fit(train_loader, validation_data=test_loader, epochs=100)
model.evaluate(test_loader)
